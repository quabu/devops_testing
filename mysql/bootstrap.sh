#!/bin/bash
#!/bin/bash
set -e
service mysql start
mysql < /mysql/sample_data.sql
service mysql stop
